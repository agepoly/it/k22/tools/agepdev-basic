# AGEPDev Basic

AGEPDev images are Docker images for in-cluster development at AGEPoly.
This image creates a simple development environment with basic tools (bash)

Base image: debian:14.4 (Bookworm)